REAL*8 FUNCTION hermite(v,x)
	! !INPUT/OUTPUT PARAMETERS:
	!   v : order of Hermite polynomial (in,integer)
	!   x : real argument (in,real)	
	! arguments
	integer, intent(in) :: v
	real(8), intent(in) :: x
	! local variables
	integer i
	real(8) h1,h2,ht
	if (v.lt.0) then
	  write(*,*)
	  write(*,'("Error(hermite): v < 0 : ",I8)') v
	  write(*,*)
	  stop
	end if
	if (v.gt.20) then
	  write(*,*)
	  write(*,'("Error(hermite): v out of range : ",I8)') v
	  write(*,*)
	  stop
	end if
	if (abs(x).gt.1.d15) then
	  write(*,*)
	  write(*,'("Error(hermite): x out of range : ",G18.10)') x
	  write(*,*)
	  stop
	end if
	if (v.eq.0) then
	  hermite=1.d0
	  return
	end if
	if (v.eq.1) then
	  hermite=2.d0*x
	  return
	end if
	h1=2.d0*x
	h2=1.d0
	do i=2,v
	  ht=2.d0*x*h1-2.d0*dble(i-1)*h2
	  h2=h1
	  h1=ht
	end do
	hermite=h1
return
END FUNCTION hermite
