# ============================================================================
# Name        : Makefile
# Author      : 
# Version     :
# Copyright   : Your copyright notice
# Description : Makefile for Hello World in Fortran
# ============================================================================

.PHONY: all clean

# Change this line if you are using a different Fortran compiler
FORTRAN_COMPILER = ifort

bin/numerov: src/numerov.f90  modnumerov.o
	$(FORTRAN_COMPILER) -g -check all -traceback\
	 -fpe0  -debug extended -I mod\
		-o bin/numerov modnumerov.o\
		src/numerov.f90

modnumerov.o: src/modnumerov.f90 	
	$(FORTRAN_COMPILER) -g -check all -traceback\
	 -fpe0  -debug extended \
	 -c src/modnumerov.f90	  
pot.png: fort.77  fort.78 fort.79 fort.80 fort.81
	gnuplot pot.gnp -
fort.77: bin/numerov
	./bin/numerov	
fort.78: bin/numerov
	./bin/numerov	
fort.79: bin/numerov
	./bin/numerov	
fort.80: bin/numerov
	./bin/numerov	
fort.81: bin/numerov
	./bin/numerov	
clean:
	rm -f bin/numerov *.mod *.png fort.* *.o
