! ============================================================================
! Name        : numerov.f90
! Author      : François Dion
! Version     : 0.1
! Copyright   : GPL
! Description : Numerov in Fortran
! ============================================================================

program numerov
    use modnumerov
    use corenumerov
    implicit none

    integer :: i,j,npoints,noeuds,nvmax,iter
    real*8 :: dx,koh,x0,xmin,xmax,Emilieu,Emin,Emax,w,pi,hbar,norm,morsepot,integral
    real*8 :: dE,seuil,m,alpha, A,dmorse,iplusnv
    real*8, dimension(:),allocatable :: pot,x,psi,psi2,E,psiexact,vnorm
    real*8, dimension(:,:),allocatable :: etatpro
    logical :: found
    real*8 :: harmpot
    ! 1 a.u =   27.211396 eV   =  219 474.63 05 cm-1 
    ! selon http://cccbdb.nist.gov/exp2.asp?casno=1333740
    ! freq H2 = 4401 cm-1
    !w=4401.d0/219474.6305d0
    pi=dacos(-1.d0)
    hbar=1.d0
    write(*,'("pi="E40.33)') pi
    m=1.d0
    !koh=w**2.d0*m*(2.d0*3.141592654)**2.d0
    w=1.d0
    !hbar=1.d0/(2.d0*pi)
    alpha=m*w/hbar
    !koh=1.d0
    nvmax=7


    xmin=-5.d0
    xmax=5.d0
    !x0=0.76d0
    x0=0.d0
    alpha=1.85d0
    npoints=2048
    Dmorse=2.d0
    !A=116900.d0
    A=0.d0
    !koh=1.d0
    allocate(E(0:nvmax))
    allocate(pot(npoints))
    allocate(x(npoints))
    allocate(psi(npoints),psiexact(npoints),vnorm(npoints))
    allocate(psi2(npoints))
    allocate(etatpro(nvmax+1,npoints))
    dx=(xmax-xmin)/dfloat(npoints-1)
    open(32,file="pot.dat",status='unknown')
    do i=1,npoints
        x(i)=xmin+(i-1)*dx
        pot(i)=harmpot(m,w,x(i),x0)
        !pot(i)=morsepot(A,Dmorse,alpha,x(i),x0)
        write(32,*) x(i),pot(i)
    end do
    close(32)
    !    Emilieu=4.999d-1
    !    dE=1.d-7
    !    seuil=1.d-5
    !
    !    call scanlineaire(psi,Emilieu,dE,1000000,seuil,pot,npoints,found,noeuds,dx,0,iter)
    !    if (found) then
    !        write(999,*)"psi0 (methode lineaire) trouv�� avec",iter,"iterations"
    !        do i=1,npoints
    !            write(76,*)x(i),psi(i)+Emilieu,pot(i)
    !        end do
    !    else
    !        do i=1,npoints
    !            write(76,*)x(i),psi(i)+Emilieu,pot(i)
    !        end do
    !
    !    end if
    Emin=0.d0
    Emax=pot(1)
    seuil=1.d-9
    do j=0,nvmax
        Emax=pot(npoints)
        E(j)=hbar*w/2.d0 ! erreur ici
        call windowsmethod(psi,Emin,Emax,1000000,seuil,pot,npoints,found,noeuds,dx,j,iter,m,hbar)
        !call schr(E(j),xmin,xmax,npoints,10000,1.d-9,pot,psi,Emilieu,j,0,1.d0,0.d0)
        psi=psi*(-1)**j
        if (found) then
            write(999,*)"Psi(",j,") trouv�� avec",iter,"iterations"
            norm=0.d0
            do i=1,npoints
                psiexact(i)=hermite(j,dsqrt(alpha)*x(i))*dexp(-alpha*x(i)**2.d0/2.d0) ! to be done
                norm=norm+dabs(psiexact(i))**2

            end do
            norm=norm*dx
            psiexact=psiexact/dsqrt(norm)
            do i=1,npoints
                write(77+j,'(6e26.6)')x(i),psi(i)+Emin,psiexact(i)+Emin,pot(i),Emin,E(j)
            end do
        else
            do i=1,npoints
                write(77+j,*)x(i),psi(i)+Emin,pot(i)
            end do
        end if
    end do
    !write(*,*) "NV=1"
    !Emin=0.d0

    !   do nv=1,nvmax+1
    !       write(64,*) E(nv)
    !   end do
    !   do i=1,npoints
    !       write(65,'(7e23.6,2x)') x(i),(etatpro(j,i),j=1,nvmax+1)
    !   end do
    xmin=0.2d0
    xmax=25.d0
    x0=2.d0
    dx=(xmax-xmin)/dfloat(npoints-1)
!    deallocate(E,pot,x,psi,psi2,psiexact,vnorm,etatpro(0:nvmax))
!    allocate(pot(npoints))
!    allocate(x(npoints))
!    allocate(psi(npoints),psiexact(npoints),vnorm(npoints))
!    allocate(psi2(npoints))
!    allocate(etatpro(nvmax+1,npoints))
    open(32,file="morsepot.dat",status='unknown')
    do i=1,npoints
        x(i)=xmin+(i-1)*dx
        !pot(i)=morsepot(0.d0,.1026277d0,0.72d0,x(i),x0)*1.d2
        pot(i)=morsepot(A,Dmorse,alpha,x(i),x0)
        write(32,*) x(i),pot(i)
    end do
    close(32)


    Emin=minval(pot)
    Emax=0.d0
        psi(1)=0.d0
    psi2(1)=0.d0
    psi(2)=1.d-20
    psi2(2)=psi(2)**2.d0
    do i=3,npoints
        psi(i)=numerovplus(psi(i-2),psi(i-1),2.d0*m*(pot(i-2)-Emax)/hbar**2.d0,2.d0*m*(pot(i-1)-Emax)/hbar**2.d0,2.d0*m*(pot(i)-Emax)/hbar**2.d0,dx)
        !psi(i)=numerovplus(psi(i-2),psi(i-1),(pot(i-2)-Emax),(pot(i-1)-Emax),(pot(i)-Emax),dx)
        !write(*,*) 'psi(i)',psi(i)
        psi2(i)=dabs(psi(i))**2.d0
    end do
    noeuds=0
    integral=simpson(psi2,1,npoints,dx)
    do i=1,npoints
        psi(i)=psi(i)/dsqrt(integral)
        psi2(i)=abs(psi(i))**2.d0
        if (i.gt.2) then
            if(((psi(i).ge.0.d0).and.(psi(i-1).le.0.d0)).or.(psi(i).le.0.d0).and.(psi(i-1).ge.0.d0)) then
                noeuds=noeuds+1
            end if
        end if
    end do
    nvmax=noeuds
    write(*,*) 'nvmax=',nvmax
    call windowsmethod(psi,Emin,Emax,1000000,seuil,pot,npoints,found,noeuds,dx,0,iter,m,hbar)
    do i=1,npoints
        write(177,'(4e26.6)')x(i),psi(i),pot(i),Emin
    end do

    deallocate(pot)
    deallocate(x)
    deallocate(psi)
end program numerov


real*8 function harmpot(m,w,x,x0)
    real*8 :: m,w,x,x0
    harmpot=m*w**2.d0/2.d0*(x-x0)**2.d0
    return
end function harmpot

real*8 function morsepot(A,De,alpha,x,x0)
    real*8 :: alpha,x,x0,A,De
    !morsepot=A+De*dexp(-2*alpha*((x-x0)))-2*De*dexp(-alpha*(x-x0))
    write(*,*)x,x0,A,De,alpha
    morsepot=A+De*((1-dexp(-alpha*(x-x0)))**2.d0-1.d0)
    return

end function morsepot



