module corenumerov
    implicit none

contains
    ! Copyright (C) 2002-2005 J. K. Dewhurst, S. Sharma and C. Ambrosch-Draxl.
    ! This file is distributed under the terms of the GNU Lesser General Public
    ! License. See the file COPYING for license details.

    !BOP
    ! !ROUTINE: hermite
    ! !INTERFACE:
    real(8) function hermite(n,x)
        ! !INPUT/OUTPUT PARAMETERS:
        !   n : order of Hermite polynomial (in,integer)
        !   x : real argument (in,real)
        ! !DESCRIPTION:
        !   Returns the $n$th Hermite polynomial. The recurrence relation
        !   $$ H_i(x)=2xH_{i-1}(x)-2nH_{i-2}(x), $$
        !   with $H_0=1$ and $H_1=2x$, is used. This procedure is numerically stable
        !   and accurate to near machine precision for $n\le 20$.
        !
        ! !REVISION HISTORY:
        !   Created April 2003 (JKD)
        !EOP
        !BOC
        implicit none
        ! arguments
        integer, intent(in) :: n
        real(8), intent(in) :: x
        ! local variables
        integer i
        real(8) h1,h2,ht
        if (n.lt.0) then
            write(*,*)
            write(*,'("Error(hermite): n < 0 : ",I8)') n
            write(*,*)
            stop
        end if
        if (n.gt.20) then
            write(*,*)
            write(*,'("Error(hermite): n out of range : ",I8)') n
            write(*,*)
            stop
        end if
        if (abs(x).gt.1.d15) then

            write(*,*)
            write(*,'("Error(hermite): x out of range : ",G18.10)') x
            write(*,*)
            stop
        end if
        if (n.eq.0) then
            hermite=1.d0
            return
        end if
        if (n.eq.1) then
            hermite=2.d0*x
            return
        end if
        h1=2.d0*x
        h2=1.d0
        do i=2,n
            ht=2.d0*x*h1-2.d0*dble(i-1)*h2
            h2=h1
            h1=ht
        end do
        hermite=h1
        return
    end function
    !EOC
    real*8 function numerovplus(psi1,psi2,G1,G2,G3,s)

        real*8 :: psi1,psi2,s,G1,G2,G3,num,denom,c3,c2,c1,Y(3),H2,HV,psi3
        !numerovplus=2.d0-s**2.d0*G2*psi2-psi1
        num=psi2*2.d0-psi1+5.d0*G2*psi2*s**2.d0/6.d0+G1*psi1*s**2.d0/12.d0
        denom=1-G3*s**2.d0/12.d0
        numerovplus=num/denom
            !c3 = 1.0+G3*s*s/12.0
            !c2 = 2.0-(5.0/6.0)*s*s*G2
            !c1 = 1.0+G1*s*s/12.0

            !numerovplus = (c2*psi2 - c1*psi1)/c3
        !  write(*,300) i+1, x(i+1), wl(i+1)
        !H2=s**2
        !HV=H2/12.D0
        !Y(2)=(1.D0-HV*G2)*psi2
        !Y(3)=Y(2)+((Y(2)-Y(1))+H2*G1*Psi1)
        !numerovplus=(2.d0 * (1.d0 - 5.d0 * s**2.d0 * G2/12.d0) * psi2 - (1.d0 + s**2.d0/12.d0 * G1))/(1 + s**2.d0/12.d0 * G3);
        !numerovplus=Y(3)/(1.D0-HV*G3)
    end function numerovplus


    Real*8 function simpson(f,k1,k2,dt) !
        INTEGER i1,i2,k,k1,k2
        real*8 sum,Sum1,Sum2,dt
        real*8 F(*)


        i1=k1
        i2=k2

        sum=0.D0
        if (mod(i2-i1,2).ne.0) then
            sum=0.5D0*dt*(f(i1)+f(i1+1))
            i1=i1+1
        end if

        Sum1=0.D0
        DO K=i1+1,i2-1,2  !  *4
            Sum1=Sum1+F(k)
        ENDDO
        Sum2=0.D0
        DO K=i1+2,i2-2,2  ! *2
            Sum2=Sum2+F(k)
        ENDDO
        sum=sum+dt*(F(i1)+F(i2)+4.D0*Sum1+2.D0*Sum2)/3.D0
        simpson=sum
        return
    END function simpson
end module corenumerov

module modnumerov
    implicit none
contains
    subroutine windowsmethod(psi,Emin,Emax,itermax,seuil,pot,npts,found,noeuds,dx,v,iter,m,hbar)
        use corenumerov
        integer :: itermax,npts,iter,noeuds,i,v
        real*8 :: psi(npts),pot(npts),Emin,Emax,psi2(npts),E,integral,dx,seuil,hbar,m
        logical :: found
        found=.false.
        iter=0

76  continue
    if((Emax-Emin)<seuil) then
        found=.true.
        go to 77
    end if
    E=Emin+(Emax-Emin)/2.d0
    iter=iter+1
    psi(1)=0.d0
    psi2(1)=0.d0
    psi(2)=1.d-20
    psi2(2)=psi(2)**2.d0
    do i=3,npts
        !psi(i)=numerovplus(psi(i-2),psi(i-1),2.d0*m*(pot(i-2)-E)/hbar**2.d0,2.d0*m*(pot(i-1)-E)/hbar**2.d0,2.d0*m*(pot(i)-E)/hbar**2.d0,dx)
        psi(i)=numerovplus(psi(i-2),psi(i-1),2.d0*(pot(i-2)-E),2.d0*(pot(i-1)-E),2.d0*(pot(i)-E),dx)
        !write(*,*) 'psi(i)',psi(i)
        psi2(i)=dabs(psi(i))**2.d0
    end do
    noeuds=0
    integral=simpson(psi2,1,npts,dx)
    do i=1,npts
        psi(i)=psi(i)/sqrt(integral)
        psi2(i)=abs(psi(i))**2.d0
        if (i.gt.2) then
            if(((psi(i).ge.0.d0).and.(psi(i-1).le.0.d0)).or.(psi(i).le.0.d0).and.(psi(i-1).ge.0.d0)) then
                noeuds=noeuds+1
            end if
        end if
    end do
    integral=simpson(psi2,1,npts,dx)
    !write(*,*) 'integral=',integral
    if (noeuds.lt.v) then
!        write(*,*) 'E est trop petit'
        Emin=E
        write(*,*) '1',Emin,'<E<',Emax
        go to 76
    else if(noeuds.gt.v) then
!        write(*,*) 'E est trop grand'
        Emax=E
        write(*,*) '2',Emin,'<E<',Emax
        go to 76
    else if (psi(npts)*(-1.d0)**v>0.d0) then
!        write(*,*) 'E est trop petit'
        Emin=E
        write(*,*) '3',Emin,'<E<',Emax
        go to 76
    else if ((psi(npts)*(-1.d0)**v).lt.0.d0) then
!        write(*,*) 'E est trop grand'
        Emax=E
        write(*,*) '4',Emin,'<E<',Emax
        go to 76
    end if
77 continue


   end subroutine windowsmethod




   subroutine scanlineaire(psi,Emin,dE,itermax,seuil,pot,npts,found,noeuds,dx,v,iter)
       use corenumerov
       integer :: itermax,npts,iter,noeuds,i,v
       real*8 :: psi(npts),pot(npts),Emin,dE,psi2(npts),E,integral,dx,seuil
       logical :: found
       found=.false.
       iter=0
       E=Emin
76 continue

   iter=iter+1
   psi(1)=0.d0
   psi2(1)=0.d0
   psi(2)=1.d-3
   psi2(2)=psi(2)**2.d0
   do i=3,npts
       psi(i)=numerovplus(psi(i-2),psi(i-1),2.d0*pot(i-2)-2.d0*E,2.d0*pot(i-1)-2.d0*E,2.d0*pot(i)-2.d0*E,dx)
       psi2(i)=abs(psi(i))**2.d0
   end do
   noeuds=0
   integral=simpson(psi2,1,npts,dx)
   do i=1,npts
       psi(i)=psi(i)/sqrt(integral)
       psi2(i)=abs(psi(i))**2.d0
       if (i.gt.2) then
           if(((psi(i).ge.0.d0).and.(psi(i-1).le.0.d0)).or.(psi(i).le.0.d0).and.(psi(i-1).ge.0.d0)) then
               noeuds=noeuds+1
           end if
           if(noeuds.ge.(v+1)) then
!               write(*,*) "E trop grand, noeuds=",noeuds
               go to 77
           else if (noeuds.le.(v-1)) then
!               write(*,*) "E trop petit, noeuds=",noeuds
               go to 77
           end if
       end if
   end do
   integral=simpson(psi2,1,npts,dx)
   !write(*,*) 'integral=',integral
   if (abs(psi(npts)).gt.seuil) then
       E=E+dE
       write(*,*) 'Emilieu=',E,'abs',abs(psi(npts)),noeuds
       !t=it+1
       go to 76
   else
       found=.true.
   end if

77 continue


   end subroutine scanlineaire
   ! Copyright (C) 2002-2005 J. K. Dewhurst, S. Sharma and C. Ambrosch-Draxl.
   ! This file is distributed under the terms of the GNU Lesser General Public
   ! License. See the file COPYING for license details.

   !BOP
   ! !ROUTINE: hermite
   ! !INTERFACE:
!   real(8) function hermite(n,x)
!       ! !INPUT/OUTPUT PARAMETERS:
!       !   n : order of Hermite polynomial (in,integer)
!       !   x : real argument (in,real)
!       ! !DESCRIPTION:
!       !   Returns the $n$th Hermite polynomial. The recurrence relation
!       !   $$ H_i(x)=2xH_{i-1}(x)-2nH_{i-2}(x), $$
!       !   with $H_0=1$ and $H_1=2x$, is used. This procedure is numerically stable
!       !   and accurate to near machine precision for $n\le 20$.
!       !
!       ! !REVISION HISTORY:
!       !   Created April 2003 (JKD)
!       !EOP
!       !BOC
!       implicit none
!       ! arguments
!       integer, intent(in) :: n
!       real(8), intent(in) :: x
!       ! local variables
!       integer i
!       real(8) h1,h2,ht
!       if (n.lt.0) then
!           write(*,*)
!           write(*,'("Error(hermite): n < 0 : ",I8)') n
!           write(*,*)
!           stop
!       end if
!       if (n.gt.20) then
!           write(*,*)
!           write(*,'("Error(hermite): n out of range : ",I8)') n
!           write(*,*)
!           stop
!       end if
!       if (abs(x).gt.1.d15) then
!           write(*,*)
!           write(*,'("Error(hermite): x out of range : ",G18.10)') x
!           write(*,*)
!           stop
!       end if
!       if (n.eq.0) then
!           hermite=1.d0
!           return
!       end if
!       if (n.eq.1) then
!           hermite=2.d0*x
!           return
!       end if
!       h1=2.d0*x
!       h2=1.d0
!       do i=2,n
!           ht=2.d0*x*h1-2.d0*dble(i-1)*h2
!           h2=h1
!           h1=ht
!       end do
!       hermite=h1
!       return
!   end function
!   !EOC
      SUBROUTINE SCHR(E0,RMIN,RMAX,N,MAXIT,EPS,V,P,E2,KV,ITRY,CV,RMUAT)
      IMPLICIT real*8 (A-H,O-Z)
      IMPLICIT INTEGER (I-N)
      DIMENSION V(N),P(N),Y(3)
  !    write(*,*) 'In SCHR'
  !    ITRY=0
      H=(RMAX-RMIN)/DFLOAT(N-1)
      H2=H**2
      HV=H2/12.D0
      E=E0
 !     write(*,*)'E=',E
      TEST=-1.D0
      DE=0.D0
      DO 1 I=1,N
!       write(*,*) 'I=',I
!C      S(I)=0.D0
1     P(I)=0.D0
!C     BOUCLE DES ITERATIONS
   12 DO 171 IT=1,MAXIT
!      write(*,*) 'IT=',IT
      XIT=IT
!C     INTEGRATION VERS L-INTERIEUR.PREMIERS PAS
      P(N)=1.D-30
      GN=V(N)-E
      GI=V(N-1)-E
1122  FORMAT(2X,'GN = ',D16.8,2X,'GI =',D16.8/)
!C     E EST-IL TROP GRAND
      IF(GI.GE.0.D0) GO TO 36
!C     IF(IT.EQ.MAXIT)GO TO 900
      E=V(N-2)
!C     PRINT 901,E
  901 FORMAT(1H ,'E EST TROP GRAND ON LE REMPLACE PAR V(N-2) = ',D13.6)
      GO TO 36
  900 PRINT 899
  899 FORMAT('LA TECHNIQUE UTILISEE EST EN DEFAUT')
      ITRY=1
  914 FORMAT(1H ,5(5X,D13.6))
      RETURN
36    APR=(RMAX-H)*DSQRT(GI)-RMAX*DSQRT(GN)
!      WRITE(*,*) 'GI.GE.0.D0'
      IF(APR.GT.50.D0) APR=50.D0
      P(N-1)=P(N)*DEXP(-APR)
38    Y(1)=(1.D0-HV*GN)*P(N)
40    Y(2)=(1.D0-HV*GI)*P(N-1)
!C
!C     INTEGRATION
   44 M=N-2
   46 Y(3)=Y(2)+((Y(2)-Y(1))+H2*GI*P(M+1))
   48 GI=V(M)-E
50    P(M)=Y(3)/(1.D0-HV*GI)
!      write(*,*) 'GROS TEST',GI
52    IF(DABS(P(M)).LT.1.D+34) GO TO 70
!C
!C     DEPASSEMENT DE LA LIMITE
      M1=M+1
      PM=P(M1)
179   FORMAT(2X,'PM = ',D16.8/)
   55 DO 56 J=M1,N
   56 P(J)=P(J)/PM
   58 Y(1)=Y(1)/PM
!C
!C     NOUVEAU DEPART
   60 Y(2)=Y(2)/PM
   62 Y(3)=Y(3)/PM
      GI=V(M+1)-E
      GO TO 46
!C     L-INTEGRATION VERS L-INTERIEUR EST-ELLE TERMINEE
   70 IF((DABS(P(M)).LE.DABS(P(M+1))).OR.(M.LE.2))GO TO 90
!      write(*,*) 'DABS(P(M)).LT.1.D+34'
   81 Y(1)=Y(2)
   82 Y(2)=Y(3)
   84 M=M-1
      GO TO 46
!C     L-INTEGRATION VERS L-INTERIEUR EST TERMINEE
   90 PM=P(M)
!      write(*,*)'L-INTEGRATION VERS L-INTERIEUR EST TERMINEE'
      MSAVE=M
   92 YIN=Y(2)/PM
   94 DO 96 J=M,N
   96 P(J)=P(J)/PM
!C     INTEGRATION VERS L-EXTERIEUR.PREMIERS PAS
100   P(1)=1.D-20
  102 Y(1)=0.D0
  104 GI=V(1)-E
106   Y(2)=(1.D0-HV*GI)*P(1)
!C
!C     INTEGRATION
  108 DO 132 I=2,M
  110 Y(3)=Y(2)+((Y(2)-Y(1))+H2*GI*P(I-1))
  112 GI=V(I)-E
114   P(I)=Y(3)/(1.D0-HV*GI)
116   IF(DABS(P(I)).LT.1.D+34) GO TO 130
!C     LA LIMITE A ETE DEPASSEE
  118 I1=I-1
      PM=P(I1)
      DO 120 J=1,I1
  120 P(J)=P(J)/PM
  122 Y(1)=Y(1)/PM
  124 Y(2)=Y(2)/PM
  126 Y(3)=Y(3)/PM
      GI=V(I1)-E
      GO TO 110
  130 Y(1)=Y(2)
  132 Y(2)=Y(3)
!C     L-INTEGRATION VERS L-EXTERIEUR EST TERMINEE
!C
      PM=P(M)
      IF(PM) 135,149,135
  135 YOUT=Y(1)/PM
  136 YM=Y(3)/PM
  138 DO 140 J=1,M
  140 P(J)=P(J)/PM
!C     LES DEUX BRANCHES SONT MAINTENANT RACCORDEES
!C     CORRECTION
142   DF=0.D0
  144 DO 146 J=1,N
  146 DF=DF-P(J)**2
148   F=(-YOUT-YIN+2.D0*YM)/H2+(V(M)-E)
      DOLD=DE
      IF(DABS(F).LT.1.D+37) GO TO 150
149   F=9.99999D+29
      DF=-F
      DE=DABS(0.0001D0*E)
      GO TO 152
  150 DE=-F/DF
  152 CONTINUE
  156 FORMAT(I2,5X,D16.8,5X,D16.8,5X,D16.8,5X,D16.8)
  164 EOLD=E
      E=E+DE
!      write(*,*) 'E From ',EOLD,' to ',E,'(',E/(CV*RMUAT),')'
      TEST=DMAX1((DABS(DOLD)-DABS(DE)),TEST)
      IF(TEST.LT.0.D0) GO TO 171
      IF(DABS(E-EOLD).LT.EPS)GO TO 172
  171 CONTINUE
      SCHROD=1.D0
      GO TO 173
!C     LES ITERATIONS SONT TERMINEES
172   SCHROD=0.D0
!C     LES ITERATIONS ONT CONVERGE
!C     COMPTAGE DES NOEUDS
  173 KV=0
      NL=N-2
  174 DO 192 J=3,NL
  176 IF(P(J))178,177,177
  177 IF(P(J-1))180,192,192
  178 IF(P(J-1))192,270,184
!C     NOEUD POSITIF
  180 IF(P(J+1))192,182,182
  182 IF(P(J-2))190,192,192
!C     NOEUD NEGATIF
  184 IF(P(J+1))186,192,192
  186 IF(P(J-2))192,190,190
!C     LE NOEUD EST-IL DU A UN SOUS-DEPASSEMENT
  270 IF(P(J+1))280,192,192
  280 IF(P(J-2))192,192,190
  190 KV=KV+1
  192 CONTINUE
      E2=E
!C     NORMALISATION
  200 SN=DSQRT(-H*DF)
  202 DO 204 J=1,N
  204 P(J)=P(J)/SN
  250 FORMAT(10X,'SCHR=',I1,/)
!C     ECRITURE DE LA SOLUTION
  214 FORMAT('V=',I3,5X,' E=',D16.8/)
  228 FORMAT(10X,D16.8,10X,D16.8)
      RETURN
      END SUBROUTINE SCHR

   end module modnumerov
   module fact
   contains
       function factrl(n)
      
           real(8) factrl
           integer n,j,ntop
           double precision a(100)
	
           data ntop,a(1)/0,1./
           if(n.lt.0) then
               pause 'negative factorial'
           else if(n.le.ntop) then
               factrl=a(n+1)
           else if(n.le.100) then
               do 11 j=ntop+1,n
                   a(j+1)=j*a(j)
11             continue
               ntop=n
               factrl=a(n+1)
           else
               factrl = dsqrt(6.283185307d0*n)*(n*dexp(-1.d0))**n
           endif
           return
       end function factrl


   end module fact
 


   module gen
   contains
       function nm2au(w)
           !  http://en.wikipedia.org/wiki/Atomic_units
           ! 1 au = 5.2917720859(36)×10−11 m
           !    include 'mkl_dfti_examples.fi'
           real(8) , INTENT(IN) :: w
           real(8) :: nm2au
           nm2au=w*1.d-9/5.2917720859d-11
       end function nm2au
       function cm12au(w)
           !  http://en.wikipedia.org/wiki/Atomic_units
           ! 1 au = 5.2917720859(36)×10−11 m
           ! 8 065.544 45(69) cm-1
           !    include 'mkl_dfti_examples.fi'
           real(8), INTENT(IN) :: w
           real(8)  :: cm12au
           write(*,*) "w in function ", w
           !cm12au=w*5.2917720859d-9
           cm12au=w/(8065.54445d0*13.6056923d0*2.d0)
           !(27.212*8065)
           write(*,*) "cm12au in function ",  cm12au
       end function cm12au
       function laguerrel(a,n,x)
           use fact
           double precision laguerrel
           integer a,n,j
           double precision x,lagtmp
	
           lagtmp=0.d0
           do 22 j=0,n
               lagtmp=lagtmp+(1.d0/factrl(j))*((-x)**j)*factrl(n+a)/(factrl(n-j)*factrl(j+a))
22         continue
           laguerrel=lagtmp
           return
       end function laguerrel
   end module gen


   module morse1
   contains
       function morse(diss,smalla,xmu,requ,r,nu)
           !
           use gen
           use fact
           real(8) :: morse
           real(8) :: diss,smalla,xmu
           real(8) :: biga,bigc,enu,alpha
           integer :: nu,m
           real(8) :: x,arg,r,requ
           real(8) :: norm
	
           if(r.lt.64.d0)then
               biga = (dsqrt(2.d0*xmu))/smalla
               bigc = biga*dsqrt(diss)
               enu = -((bigc-nu-.5d0)**2)/biga**2
               alpha = bigc-nu-0.5d0
               arg=dexp(-smalla*(r-requ))
               x=2.d0*bigc*arg
               m = 2*(idint(bigc)-nu)-1
               morse = laguerrel(m,nu,x)
               morse = morse * (x**(idint(bigc)))
               morse = morse / (x**nu)
               morse = morse / dsqrt(x)
               morse = morse * dexp(-x/2.d0)
	
               norm = smalla * factrl(nu) * (2.d0*bigc - 2.d0*nu - 1.d0)
               norm = norm / factrl(m+nu)
               norm = dsqrt(norm)
               morse = morse*norm
           else
               morse = 0.d0
           endif
           return
       end function morse
   end module morse1

